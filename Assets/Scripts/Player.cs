using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour {
    public float horizontalForceMultiplier = 10f;
    public float uprightForceMultiplier = 3f;
    public Transform[] gravityRays;
    public LayerMask gravityRayLayerMask;

    private float horizontalInput = 0f;
    private Rigidbody2D body;
    private RaycastHit2D[] hits = new RaycastHit2D[10];

    private void Awake() {
        body = GetComponent<Rigidbody2D>();
    }

    private void Update() {
        horizontalInput = Input.GetAxis("Horizontal");
    }

    private void FixedUpdate() {
        ControlFunctions();
        GravityRays();
    }

    private void GravityRays() {
        var closestHit = new RaycastHit2D();
        closestHit.distance = 100f;
        foreach (var gravityRay in gravityRays) {
            var hitCount = Physics2D.RaycastNonAlloc(gravityRay.position, gravityRay.up, hits, 10f, gravityRayLayerMask.value);
            for (int i = 0; i < hitCount; i++) {
                var hit = hits[i];
                if (hit.distance < closestHit.distance) {
                    closestHit = hit;
                }
            }
            Gravity.SetGravity(-closestHit.normal);
        }
    }

    private void ControlFunctions() {
        //horizontal input
        var gravity = Physics2D.gravity;
        var horizon = Vector2.Perpendicular(gravity);
        var horizontalInputForce = horizon * (horizontalForceMultiplier * horizontalInput);
        body.AddForce(horizontalInputForce);

        //upright force
        var down = -transform.up;
        var uprightTorque = Vector2.SignedAngle(down, gravity) * uprightForceMultiplier;
        body.AddTorque(uprightTorque);
    }
}
