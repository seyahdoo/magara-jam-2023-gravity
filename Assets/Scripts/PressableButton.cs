using LapsRuntime;
using UnityEngine;

public class PressableButton : LapsComponent
{
    private bool inside = false;
    public LayerMask triggeringLayers;
    
    public override void GetOutputSlots(SlotList slots) {
        slots.Add(new LogicSlot("On Pressed", 0));
    }

    private void Update() {
        if (inside && Input.GetKeyDown(KeyCode.E)) {
            FireOutput(0);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (triggeringLayers == (triggeringLayers | (1 << other.gameObject.layer))) {
            inside = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other) {
        if (triggeringLayers == (triggeringLayers | (1 << other.gameObject.layer))) {
            inside = false;
        }
    }
}
